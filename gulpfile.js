var gulp = require("gulp"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps");


var paths = {
  styles: {
    src: "scss/*.scss",
    dest: "css"
  }
};

function style() {
  return (
    gulp
      .src("scss/*.scss")
      .pipe(sourcemaps.init())
      .pipe(sass())
      .on("error", sass.logError)
      .pipe(postcss([autoprefixer(), cssnano()]))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest("css"))
  );
}

exports.style = style;

function watch() {
  style();
  gulp.watch(paths.styles.src, style);
}

exports.watch = watch
