import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Title, Meta } from '@angular/platform-browser';

import { RestService } from './rest.service';
import { WordpressService } from './wordpress.service';
import { CookieService } from 'ngx-cookie-service';

import { Observable, Subject, forkJoin } from 'rxjs';
import { map, catchError, tap, mergeMap, first } from 'rxjs/operators';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  // Set all of our blank variables to fill with data.
  cookieValue = 'UNKNOWN';

  title = 'Luceco';
  wpPages:any = [];

  companyInfo:any = [];
  compLogo:any = [];
  compPhone:any = [];
  compEmail:any = [];

  compStreet:any = [];
  compCity:any = [];
  compCountry:any = [];
  compPostCode:any = [];

  language:any = [
    { name: "en", id: "1" },
    { name: "fr", id: "2" },
    { name: "es", id: "3" },
  ];

  searchResult:any = [];
  searchAttributes:any = [];

  constructor(private wp: WordpressService, public rest:RestService, private route: ActivatedRoute, private router: Router, private cookieService: CookieService, private titleService: Title, private metaTagService: Meta) { }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
    this.metaTagService.updateTag(
      {
        name: 'description',
        content: "This is my product archive meta description."
      }
    );
  }

  ngOnInit() {
    this.getCompanyInfo();
    this.getPages();
  }

  // Logic to control setting/checking the language cookie.
  setLanguage(name) {
    const cookieExists: boolean = this.cookieService.check('wpLanguage');
    if(cookieExists) {
      this.cookieService.delete('wpLanguage');
      this.cookieService.set('wpLanguage', name);
      window.location.reload();
    } else {
      this.cookieService.set('wpLanguage', name);
      window.location.reload();
    }
  }

  // Return company information from the route Electrika node.
  getCompanyInfo() {
    this.companyInfo = [];
    this.rest.getCompanyInfo().subscribe((data) => {
      this.companyInfo = data;

      let compLogo = data.find((c) => c.Title === 'Logo');
      let compPhone = data.find((c) => c.Title === 'EM_Telephone');
      let compEmail = data.find((c) => c.Title === 'EM_ContactEmail');

      let compStreet = data.find((c) => c.Title === 'EM_Street');
      let compCity = data.find((c) => c.Title === 'EM_City');
      let compCountry = data.find((c) => c.Title === 'EM_Country');
      let compPostCode = data.find((c) => c.Title === 'EM_PostCode');

      this.compLogo = compLogo;
      this.compPhone = compPhone;
      this.compEmail = compEmail;
      this.compStreet = compStreet;
      this.compCity = compCity;
      this.compCountry = compCountry;
      this.compPostCode = compPostCode;

    });
  }

  // Get all WordPress pages from the WP Rest API
  getPages() {
    this.wpPages = [];
    this.wp.getPages().subscribe((data) => {
      this.wpPages = data;
    });
  }

  // Search products based on a user input string.
  // NOTE: Tried to get attributes with the search request but it crashed the browser.
  searchProduct(value: string) {
    this.searchResult = [];
    this.rest.searchProduct(value).subscribe((data: {}) => {
      this.searchResult = data;
    });
  }

}
