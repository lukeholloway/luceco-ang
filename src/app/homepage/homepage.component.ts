import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { WordpressService } from '../wordpress.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  homePage: any = [];

  yoastTitle:any = [];
  yoastDesc:any = [];

  constructor(private wp: WordpressService, private route: ActivatedRoute, private router: Router, private titleService: Title, private metaTagService: Meta) { }

  ngOnInit() {
    this.route.paramMap.subscribe(map => {

      this.wp.getHomePage().subscribe((data: {}) => {

        // Retrieve the home page data from the WordPress API
        this.homePage = data;

        // Set the page meta information
        let metaTitle = this.homePage[0]['yoast_meta'].yoast_wpseo_title;
        let metaDescription = this.homePage[0]['yoast_meta'].yoast_wpseo_metadesc;

        this.yoastTitle = metaTitle;
        this.yoastDesc = metaDescription;

        this.titleService.setTitle( this.yoastTitle );
        this.metaTagService.addTag( { name: 'description', content: this.yoastDesc } );

      });

    });
  }

}
