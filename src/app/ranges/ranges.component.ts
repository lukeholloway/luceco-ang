import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { Observable, Subject, forkJoin } from 'rxjs';
import { map, catchError, tap, mergeMap, first } from 'rxjs/operators';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/Rx';

@Component({
  selector: 'app-ranges',
  templateUrl: './ranges.component.html',
  styleUrls: ['./ranges.component.scss']
})
export class RangesComponent implements OnInit {

  // Set all of our blank variables to fill with data.
  ranges:any = [];
  rangeParts:any = [];
  productAttributes:any = [];

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router, private titleService: Title, private metaTagService: Meta) { }

  ngOnInit() {

    this.getRanges();
    this.getRangeProducts();

  }

  // Get our ranges from the root node
  getRanges() {
    this.ranges = [];
    this.rest.getRanges().subscribe((data: {}) => {
      this.ranges = data;
      console.log(this.ranges);
    });
  }

  // Get parts listed under the ranges
  getRangeProducts() {
    this.rangeParts = [];
    this.rest.getRanges()
    .pipe(
      first(),
      mergeMap((array: Array<{ Name: string, ID: number }>) => {
        return forkJoin(array
          .map((item) => {
            return this.rest.getIndexNodes(item.ID);
          })
        );
      })
    )
    .subscribe((results) => {
      this.rangeParts = results;
    });
  }

  // Return attributes of the current node ID
  getAttributes(id) {
    this.productAttributes = [];
    this.rest.getAttributes(id).subscribe((data) => {
      this.productAttributes = data;
    });
  }

}
