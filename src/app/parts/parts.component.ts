import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { Observable, Subject, forkJoin } from 'rxjs';
import { map, catchError, tap, mergeMap, first } from 'rxjs/operators';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/Rx';

@Component({
  selector: 'app-parts',
  templateUrl: './parts.component.html',
  styleUrls: ['./parts.component.scss']
})
export class PartsComponent implements OnInit {

  // brandParts:any = [];
  // partAtt:any = [];

  products:any = [];
  partTree:any = [];

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router, private titleService: Title, private metaTagService: Meta) { }

  ngOnInit() {

    // Set SEO meta information for the page.
    // NOTE: will need to make this dynamic based on the part returned.
    this.titleService.setTitle( 'Product Parts' );
    this.metaTagService.updateTag({name: 'description', content: "This is my cool product meta description."});

    this.getProducts();
    this.getProductNodes();

  }

  // Get our product root
  getProducts() {
    this.products = [];
    this.rest.getProducts().subscribe((data: {}) => {
      this.products = data;
    });
  }

  // Get parts listed under the product root and return the results
  getProductNodes() {
    this.partTree = [];
    this.rest.getProducts()
    .pipe(
      first(),
      mergeMap((array: Array<{Name: string, ID: number}>) => {
        return forkJoin(array.map((item) => {
          return this.rest.getIndexNodes(item.ID);
        }));
      })
    )
    .subscribe((results) => {
      this.partTree = results;
      console.log(this.partTree);
    });
  }


}
