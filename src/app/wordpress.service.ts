import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class WordpressService {


  languageVar;
  wp_api;

  constructor(private http: HttpClient, private cookieService: CookieService) {
    // Set the users desired language with the help of a cookie.
    // Reference: https://www.npmjs.com/package/ngx-cookie-service
    cookieService.get('wpLanguage');
    const value: string = cookieService.get('wpLanguage');
    if(value === 'en') {
      this.languageVar = '';
    } else {
      this.languageVar = '/' + value;
    }
    this.wp_api = 'https://lucecodev.wpengine.com' + this.languageVar + '/wp-json/wp/v2/';
  }


  private extractData(res: Response) {
    let body = res;
    return body || { };
  }


  // Get full list of published pages from WordPress
  getPages(): Observable<any[]> {
    return this.http.get<any[]>(this.wp_api + 'pages?_embed', {
      params: {
        status: 'publish'
      }
    });
  }

  // Return single page data from WordPress
  getSinglePage(id): Observable<any> {
    return this.http.get(this.wp_api + "pages?slug=" + id).pipe(
      map(this.extractData)
    );
  }

  // Return the latest six posts from our WordPress blog
  getPosts(): Observable<any[]> {
    return this.http.get<any[]>(this.wp_api + 'posts?_embed', {
      params: {
        per_page: '6'
      }
    });
  }

  // Get the single blog post content by current route ID
  getSingle(id): Observable<any> {
    return this.http.get(this.wp_api + "posts?slug=" + id).pipe(
      map(this.extractData)
    );
  }


  // Get the home page from WordPress
  getHomePage(): Observable<any> {
    return this.http.get(this.wp_api + "pages?slug=home").pipe(
      map(this.extractData)
    );
  }


}
