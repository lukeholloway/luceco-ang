import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { NewsComponent } from './news/news.component';

import { WordpressService } from './wordpress.service';
import { RestService } from './rest.service';
import { NewsSingleComponent } from './news-single/news-single.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PageSingleComponent } from './page-single/page-single.component';
import { PartsComponent } from './parts/parts.component';
import { RangesComponent } from './ranges/ranges.component';

import { CookieService } from 'ngx-cookie-service';


const appRoutes: Routes = [
  { path: '',
    component: HomepageComponent,
    pathMatch: 'full'
  },
  {
    path: 'products',
    component: ProductComponent,
    data: { title: 'Products' }
  },
  {
    path: 'product-options/640609',
    redirectTo: 'products'
  },
  {
    path: 'product-options/998498',
    redirectTo: 'ranges'
  },
  {
    path: 'product-options/:id',
    component: ProductDetailComponent,
    data: { }
  },
  {
    path: 'parts',
    component: PartsComponent,
    data: { }
  },
  {
    path: 'ranges',
    component: RangesComponent,
    data: { }
  },
  {
    path: 'news',
    component: NewsComponent,
    data: { }
  },
  {
    path: 'news/:id',
    component: NewsSingleComponent,
    data: { }
  },
  {
    path: 'page/home',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'page/:id',
    component: PageSingleComponent,
    pathMatch: 'full'
  },
  // { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    ProductDetailComponent,
    NewsComponent,
    NewsSingleComponent,
    HomepageComponent,
    PageSingleComponent,
    PartsComponent,
    RangesComponent
  ],
  imports:[
    CommonModule,
    NgtUniversalModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule
  ],
  providers: [
    WordpressService,
    RestService,
    CookieService,
    Title
  ],
})
export class AppModule {
}
