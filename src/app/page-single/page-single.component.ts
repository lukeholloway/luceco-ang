import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { WordpressService } from '../wordpress.service';

@Component({
  selector: 'app-page-single',
  templateUrl: './page-single.component.html',
  styleUrls: ['./page-single.component.scss']
})
export class PageSingleComponent implements OnInit {

  // Set all of our blank variables to fill with data.
  singlePage:any = [];

  yoastTitle:any = [];
  yoastDesc:any = [];

  constructor(private wp: WordpressService, private route: ActivatedRoute, private router: Router,  private titleService: Title, private metaTagService: Meta) { }

  ngOnInit() {

    this.route.paramMap.subscribe(map => {

      // Return the current single page data from the WordPress API
      this.wp.getSinglePage(this.route.snapshot.params['id']).subscribe((data) => {

        // Retreive the single page content from the WordPress API
        this.singlePage = data;

        // Set the page meta data
        let metaTitle = this.singlePage[0]['yoast_meta'].yoast_wpseo_title;
        let metaDescription = this.singlePage[0]['yoast_meta'].yoast_wpseo_metadesc;

        this.yoastTitle = metaTitle;
        this.yoastDesc = metaDescription;

        this.titleService.setTitle( this.yoastTitle );
        this.metaTagService.addTag( { name: 'description', content: this.yoastDesc } );

      });

    });

  }

}
