import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { Observable, Subject, forkJoin } from 'rxjs';
import { map, catchError, tap, mergeMap, first } from 'rxjs/operators';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/Rx';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {

  // Set all of our blank variables to fill with data.
  seoTitle:any = [];
  subProduct:any = [];
  subAttributes:any = [];
  product:any = [];
  productChildren:any = [];
  attributes:any = [];
  breadcrumbs:any = [];
  components:any = [];
  accessories:any = [];
  files:any = [];
  pageName:any = [];
  mainImage:any = [];
  productDescription:any = [];
  cataloguePage:any = [];
  per:any = [];
  unit:any = [];
  tradePrice:any = [];
  catalogueName:any = [];
  description:any = [];
  warranty:any = [];
  ipRating:any = [];
  imageTwo:any = [];
  imageThree:any = [];
  imageFour:any = [];
  featOne:any = [];
  featTwo:any = [];
  featThree:any = [];
  featFour:any = [];
  featFive:any = [];
  parentNodeId:any = [];
  testNode:any = [];
  subDatasheets:any = [];

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router, private titleService: Title, private metaTagService: Meta) {}

  ngOnInit() {

    // Map everything to the current route ID of the page.
    // This set dynamically by our links.
    this.route.paramMap.subscribe(map => {

      // Find products by current node ID
      this.rest.getProduct(this.route.snapshot.params['id']).subscribe((data) => {
        this.product = data;
        this.seoTitle = this.product.Name;

        // Set SEO meta information for the page.
        this.titleService.setTitle( this.seoTitle + " | Luceco LED Lighting" );
        this.metaTagService.updateTag({name: 'description', content: ""});

      });

      // Find child nodes for the current node ID
      this.rest.getProductChildren(this.route.snapshot.params['id']).subscribe((data: {}) => {
        this.productChildren = data;
      });

      // Return product attributes for the current node ID
      this.rest.getAttributes(this.route.snapshot.params['id']).subscribe((data) => {
        this.attributes = data;

        // Set our common attributes as variables for easier use in our template.
        let pageName = data.find((c) => c.Title === 'LUCE_Product_Page_Name');
        let imageResult = data.find((c) => c.Title === 'EC_ProductImage1_Medium');
        let descResult = data.find((c) => c.Title === 'EC_ProductDescription');
        let cataloguePage = data.find((c) => c.Title === 'EC_PDFCataloguePage');
        let per = data.find((c) => c.Title === 'EC_Per');
        let unit = data.find((c) => c.Title === 'EC_Unit');
        let tradePrice = data.find((c) => c.Title === 'EC_TradePrice');
        let catalogueName = data.find((c) => c.Title === 'LUCE_Catalogue_Name');
        let description = data.find((c) => c.Title === 'LUCE_Description');
        let warranty = data.find((c) => c.Title === 'LUCE_Warranty');
        let ipRating = data.find((c) => c.Title === 'LUCE_IP_Rating');

        // Get other product images from the attributes
        let imageResultTwo = data.find((c) => c.Title === 'EC_ProductImage2_Medium');
        let imageResultThree = data.find((c) => c.Title === 'EC_ProductImage3_Medium');
        let imageResultFour = data.find((c) => c.Title === 'EC_ProductImage4_Medium');

        // Get product features seperately
        let featOne = data.find((c) => c.Title === 'LUCE_Feature_1');
        let featTwo = data.find((c) => c.Title === 'LUCE_Feature_2');
        let featThree = data.find((c) => c.Title === 'LUCE_Feature_3');
        let featFour = data.find((c) => c.Title === 'LUCE_Feature_4');
        let featFive = data.find((c) => c.Title === 'LUCE_Feature_5');

        this.pageName = pageName;
        this.mainImage = imageResult;
        this.productDescription = descResult;
        this.cataloguePage = cataloguePage;
        this.per = per;
        this.unit = unit;
        this.tradePrice = tradePrice;
        this.catalogueName = catalogueName;
        this.description = description;
        this.warranty = warranty;
        this.ipRating = ipRating;

        this.imageTwo = imageResultTwo;
        this.imageThree = imageResultThree;
        this.imageFour = imageResultFour;

        this.featOne = featOne;
        this.featTwo = featTwo;
        this.featThree = featThree;
        this.featFour = featFour;
        this.featFive = featFive;

        this.parentNodeId = pageName;

      });

      // Return the breadcrumb for the the current node ID
      this.rest.getBreadcrumb(this.route.snapshot.params['id']).subscribe((data) => {
        // Get the array of breadcrumbs and remove the IDs that are above our desired start node.
        let myArray = data.filter(function( obj ) {
          return obj.ID !== 1;
        });
        let myArrayTwo = myArray.filter(function( obj ) {
          return obj.ID !== 587611;
        });
        let myArrayThree = myArrayTwo.filter(function( obj ) {
          return obj.ID !== 587612;
        })
        // Set the cleaned array of breadcrumb IDs without the nodes above Luceco Lighting
        this.breadcrumbs = myArrayThree;
      });

    });

    // Run these on page init
    this.getSubProducts();
    this.getSubAttributes();
    this.getSubDatasheets();

  }

  // Get Product Subcategories in relation to the curent product
  getSubProducts() {
    this.subProduct = [];
    this.rest.getProductChildren(this.route.snapshot.params['id'])
    .pipe(
      first(),
      mergeMap((array: Array<{Name: string, ID: number}>) => {
        return forkJoin(array.map((item) => {
          return this.rest.getProductChildren(item.ID);
        }));
      })
    )
    .subscribe((results) => {
      this.subProduct = results;
    });
  }

  // Get all of the attributes for our product/part.
  getSubAttributes() {
    this.subAttributes = [];
    this.rest.getProductChildren(this.route.snapshot.params['id'])
    .pipe(
      first(),
      mergeMap((array: Array<{Name: string, ID: number}>) => {
        return forkJoin(array.map((item) => {
          return this.rest.getAttributes(item.ID);
        }));
      })
    )
    .subscribe((results) => {
      this.subAttributes = results;
    });
  }

  // Retrieve the datasheets for the sub products/parts.
  getSubDatasheets() {
    this.subDatasheets;
    this.rest.getProductChildren(this.route.snapshot.params['id'])
    .pipe(
      first(),
      mergeMap((array: Array<{Name: string, ID: number}>) => {
        return forkJoin(array.map((item) => {
          return this.rest.getDatasheets(item.ID);
        }));
      })
    )
    .subscribe((results) => {
      this.subDatasheets = results;
    });
  }

}
