import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { Observable, Subject, forkJoin } from 'rxjs';
import { map, catchError, tap, mergeMap, first } from 'rxjs/operators';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/Rx';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  // Set all of our blank variables to fill with data.
  products:any = [];
  subProducts:any = [];

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router, private titleService: Title, private metaTagService: Meta) {}

  ngOnInit() {

    // Set the page meta information
    this.titleService.setTitle("Commercial & Domestic LED Products | Luceco LED Lighting");
    this.metaTagService.updateTag({name: 'description', content: "Luceco LED Lighting is a leading global manufacturer and distributor of high performing LED light and LED lighting solutions. Find out more today."});

    this.getProducts();
    this.getSubProducts();

  }

  // Get all product categories from the root of our project.
  getProducts() {
    this.products = [];
    this.rest.getProducts().subscribe((data: {}) => {
      this.products = data;
    });
  }

  // Get Product Subcategories in relation to product
  getSubProducts() {
    this.subProducts = [];
    this.rest.getProducts()
    .pipe(
      first(),
      mergeMap((array: Array<{Name: string, ID: number}>) => {
        return forkJoin(array.map((item) => {
          return this.rest.getProductChildren(item.ID);
        }));
      })
    )
    .subscribe((results) => {
      this.subProducts = results;
    });
  }

}
