import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, forkJoin } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})

export class RestService {

  apiBase;

  constructor(private http: HttpClient) {
    this.apiBase = 'https://api-test.electrika.com/api/';

  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  // Website search facility. Runs a search against the API and returns results.
  searchProduct(value: string): Observable<any> {
    return this.http.get(this.apiBase + 'SearchNodes?name=' + value + '&nodeType=&brandNode=587611&exactMatch=false').pipe(
      map(this.extractData)
    );
  }

  // Get all children of our project root ID
  getProducts(): Observable<any> {
    return this.http.get(this.apiBase + 'NodeChildren/640609').pipe(
      map(this.extractData)
    );
  }

  // Get the breadcrumb for the project root ID
  getProductsBreadcrumb(): Observable<any> {
    return this.http.get(this.apiBase + 'Breadcrumb/640609').pipe(
      map(this.extractData)
    );
  }

  // Get brand parts to display as fake single products
  getBrandParts(): Observable<any> {
    return this.http.get(this.apiBase + 'PartNodes?brandNodeId=587611&skip=0&take=1000').pipe(
      map(this.extractData)
    );
  }

  // Return the individual product/category node
  getProduct(id): Observable<any> {
    return this.http.get(this.apiBase + 'Node/' + id).pipe(
      map(this.extractData)
    );
  }

  // Return the individual product/category child nodes of the current node
  getProductChildren(id): Observable<any> {
    return this.http.get(this.apiBase + 'NodeChildren/' + id).pipe(
      map(this.extractData)
    );
  }

  // Return the parent Nodes of the current node
  getParentNodes(id): Observable<any> {
    return this.http.get(this.apiBase + 'NodeParents/' + id ).pipe(
      map(this.extractData)
    )
  }

  // Return parts associated with the current node
  getParts(id): Observable<any> {
    return this.http.get(this.apiBase + 'NodeChildren/' + id).pipe(
      map(this.extractData)
    );
  }

  // Return the individual product/category attributes related to the current node
  getAttributes(id): Observable<any> {
    return this.http.get(this.apiBase + 'NodeAttributes/' + id).pipe(
      map(this.extractData)
    );
  }

  // Return the individual product/category components related to the current node
  getComponents(id): Observable<any> {
    return this.http.get(this.apiBase + 'Components/' + id).pipe(
      map(this.extractData)
    );
  }

  // Return the individual product/category accessories related to the current node
  getAccessories(id): Observable<any> {
    return this.http.get(this.apiBase + 'Accessories/' + id).pipe(
      map(this.extractData)
    );
  }

  // Dynamically return the breadcrumb for all nodes after the root
  getBreadcrumb(id): Observable<any> {
    return this.http.get(this.apiBase + 'Breadcrumb/' + id).pipe(
      map(this.extractData)
    );
  }

  // Dynamically return the file downloads associated with the current node id
  getFiles(id): Observable<any> {
    return this.http.get(this.apiBase + 'FileDownloads/' + id).pipe(
      map(this.extractData)
    );
  }

  // Get all the company information stored in the root of the main node
  getCompanyInfo(): Observable<any> {
    return this.http.get(this.apiBase + 'NodeAttributes/587612').pipe(
      map(this.extractData)
    );
  }

  // Get all parts associated with the Node index
  getIndexNodes(id): Observable<any> {
    return this.http.get(this.apiBase + 'PartNodesWithinIndex/' + id).pipe(
      map(this.extractData)
    )
  }

  // Return all part nodes associated with Luceco Lightings root node
  lightingNodes():Observable<any> {
    return this.http.get(this.apiBase + 'PartNodesWithinIndex/640609').pipe(
      map(this.extractData)
    )
  }

  // Get datasheets from any given node
  getDatasheets(id): Observable<any> {
    return this.http.get(this.apiBase + 'Datasheets/' + id).pipe(
      map(this.extractData)
    );
  }

  // Get all ranges from the Luceco range node
  getRanges(): Observable<any> {
    return this.http.get(this.apiBase + 'NodeChildren/998503').pipe(
      map(this.extractData)
    );
  }

}
