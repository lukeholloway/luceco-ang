import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { WordpressService } from '../wordpress.service';

@Component({
  selector: 'app-news-single',
  templateUrl: './news-single.component.html',
  styleUrls: ['./news-single.component.scss']
})
export class NewsSingleComponent implements OnInit {

  singlePost: any = [];

  constructor(private wp: WordpressService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    
    this.route.paramMap.subscribe(map => {

      this.wp.getSingle(this.route.snapshot.params['id']).subscribe((data: {}) => {
        this.singlePost = data;
        console.log(data);
      });

    });

  }

}
