import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { Title, Meta } from '@angular/platform-browser';
import { WordpressService } from '../wordpress.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  posts: any = [];

  constructor(private wp: WordpressService, private route: ActivatedRoute, private router: Router, private titleService: Title, private metaTagService: Meta) {}

  ngOnInit() {

    this.titleService.setTitle("Blog");
    this.metaTagService.updateTag({name: 'description', content: "This is my cool blog meta description."});

    this.route.paramMap.subscribe(map => {

      this.wp.getPosts().subscribe((data: {}) => {
        this.posts = data;
        console.log(data);
      });

    });

  }

}
